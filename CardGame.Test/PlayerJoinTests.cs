using System;
using Xunit;
using CardGame.Engine.Aggregates;
using System.Collections.Generic;
using Xunit.Abstractions;
using Newtonsoft.Json;
using System.Linq;

namespace CardGame.Test
{
    public class PlayerJoinTests
    {
        private readonly ITestOutputHelper output;

        public PlayerJoinTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        private int _remainingSeats = -1;
        private bool _firstPlayerIsTheDealer = false;

        private string[] FOUR_PSEUDO_OF_PLAYERS = { "usul", "karimdebache", "avner", "epenser" };

        List<PlayingCard> cards_deals = new List<PlayingCard>(); //for Cards_are_correctly_dealed Test!

        /// <summary>
        /// Scenario : player join firstly a game
        /// 
        /// Given there's a new game just created
        /// When a new player join the game
        /// The game engine fire events
        /// And we can verify the remaining seats must be 3
        /// And that this first player is the dealer
        /// </summary>

        [Fact]        
        public void Player_join_firstly_a_game()
        {
            Game newGame = new Game();

            newGame.NewPlayerJoinedEvent += event_ForPlayer_join_a_game;

            var newPlayerCmd = new Commands.PlayerJoinTheGame(newGame.Id, "usul", false);
            newGame.Handle(newPlayerCmd);

            Assert.Equal(_remainingSeats, 3);
            Assert.Equal(_firstPlayerIsTheDealer, true);
        }

        /// <summary>
        /// Scenario : the player join a game who's full
        /// 
        /// Given there's a new game just created
        /// Add 4 players join this game
        /// When a 5th player join
        /// The game engine decide to throw an exception
        /// </summary>

        [Fact]
        public void System_reject_player_when_game_is_full()
        {
            Game newGame = new Game();

            newGame.NewPlayerJoinedEvent += event_ForPlayer_join_a_game;
            newGame.CardsAreDealedEvent += event_cardDealed;

            foreach (string pseudo in FOUR_PSEUDO_OF_PLAYERS)
                newGame.Handle(new Commands.PlayerJoinTheGame(newGame.Id, pseudo,false));

            var Player5_JoinCmd = new Commands.PlayerJoinTheGame(newGame.Id, "norman", false);

            Assert.Throws<DomainExceptions.PlayerOverQuotaException>(() => newGame.Handle(Player5_JoinCmd));
        }

        /// <summary>
        /// Scenario : Cards are correctly dealed 
        /// 
        /// Given there's a new game
        /// Add 4 players join this game
        /// When the cards are all dealed
        /// Then each players have 8 cards
        /// And each card is unique
        /// </summary>
        [Fact]
        public void Cards_are_correctly_dealed()
        {
            Game newGame = new Game();

            newGame.CardsAreDealedEvent += eventCardsDealed_and_we_will_verify_the_contents; 

            foreach (string pseudo in FOUR_PSEUDO_OF_PLAYERS)
                newGame.Handle(new Commands.PlayerJoinTheGame(newGame.Id, pseudo, false));   
        }

        private void eventCardsDealed_and_we_will_verify_the_contents(object sender, Events.CardsAreDealed e)
        {
            string dealErrorMessage = "Dealing error : The player don't have 8 cards in hand.";

            if (e.NorthPlayer.CardsInHand.Count != 8)
                throw new InvalidOperationException(dealErrorMessage);
            cards_deals.AddRange(e.NorthPlayer.CardsInHand);

            if (e.WestPlayer.CardsInHand.Count != 8)
                throw new InvalidOperationException(dealErrorMessage);
            cards_deals.AddRange(e.WestPlayer.CardsInHand);

            if (e.SouthPlayer.CardsInHand.Count != 8)
                throw new InvalidOperationException(dealErrorMessage);
            cards_deals.AddRange(e.SouthPlayer.CardsInHand);

            if (e.EastPlayer.CardsInHand.Count != 8)
                throw new InvalidOperationException(dealErrorMessage);
            cards_deals.AddRange(e.EastPlayer.CardsInHand);

            cards_deals = cards_deals.OrderBy(x=>x.rank).OrderBy(y=>y.suit).ToList();

            bool eachCardDealIsUnique = cards_deals.Distinct().Count() == cards_deals.Count();

            if (!eachCardDealIsUnique)
                throw new InvalidOperationException("Dealing error : There's duplicate cards in the player hands!");
        }

        private void event_cardDealed(object sender, Events.CardsAreDealed e)
        {
            output.WriteLine(JsonConvert.SerializeObject(e) + "\n");
        }

        private void event_ForPlayer_join_a_game(object sender, Events.NewPlayerJoined e)
        {
            _remainingSeats = e.RemainingPlacesCount;
            _firstPlayerIsTheDealer = e.IsDealer;
            AddAdditionnalInfosOnOutputTest(e);

        }

        private void AddAdditionnalInfosOnOutputTest(Object objectToJsonify)
        {
            output.WriteLine(JsonConvert.SerializeObject(objectToJsonify)+"\n");
        }
    }
}
