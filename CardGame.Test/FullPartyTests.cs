using CardGame.Engine.Aggregates;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace CardGame.Test
{
    public class FullPartyTests
    {
        private readonly ITestOutputHelper output;
        private Game newGame;

        public FullPartyTests(ITestOutputHelper output)
        {
            this.output = output;
            this.newGame = new Engine.Aggregates.Game();
        }
        
        private string[] FOUR_PSEUDO_OF_PLAYERS = { "usul", "karimdebache", "avner", "epenser" };

        List<List<PlayingCard>> cardDealedBySeat = new List<List<PlayingCard>>();

        List<int> cardPlayedBySeat = new List<int>() { 0, 0, 0, 0 };

        /// <summary>
        /// Scenario : full "bataille-per-Team" !
        /// 
        /// Given there's a new game just created
        /// When a 4 players join the game
        /// And each player play his first card in hand
        /// And all cards are played
        /// Then the game has to tell the winner
        /// </summary>

        [Fact]
        public void Full_bataille_per_team()
        {
            newGame.CardsAreDealedEvent += NewGame_CardsAreDealedEvent;
            newGame.PlayerHaveToPlayChangedEvent += NextPlayerHaveToPlayEvent;
            newGame.CurrentTurnCompletedEvent += NewGame_CurrentTurnCompletedEvent;
            newGame.CurrentGameCompletedEvent += NewGame_CurrentGameCompletedEvent;

            foreach (string pseudo in FOUR_PSEUDO_OF_PLAYERS)
                newGame.Handle(new Commands.PlayerJoinTheGame(newGame.Id, pseudo, false));  
        }


        private void NewGame_CardsAreDealedEvent(object sender, Events.CardsAreDealed e)
        {
            cardDealedBySeat.Add(e.NorthPlayer.CardsInHand);
            cardDealedBySeat.Add(e.WestPlayer.CardsInHand);
            cardDealedBySeat.Add(e.SouthPlayer.CardsInHand);
            cardDealedBySeat.Add(e.EastPlayer.CardsInHand);
        }

        private void NextPlayerHaveToPlayEvent(object sender, Events.PlayerHaveToPlayChanged e)
        {
            int playerSeatIndex = e.NextPlayerHaveToPlayIndexSeat;
            PlayingCard cardToPlay = cardDealedBySeat[e.NextPlayerHaveToPlayIndexSeat][cardPlayedBySeat[e.NextPlayerHaveToPlayIndexSeat]];

            cardPlayedBySeat[e.NextPlayerHaveToPlayIndexSeat] += 1; //On m�morise le fait que localement la carte est d�j� jou�e

            string log = $"Joueur {playerSeatIndex} play {cardToPlay}\n";
            output.WriteLine(log);

            newGame.Handle(new Commands.PlayerPlayCard(e.NextPlayerHaveToPlayIndexSeat, cardToPlay, newGame.Id));
        }

        private void NewGame_CurrentGameCompletedEvent(object sender, Events.CurrentGameCompleted e)
        {
            newGame.PlayerHaveToPlayChangedEvent -= NextPlayerHaveToPlayEvent;
            AddAdditionnalInfosOnOutputTest(e);
        }

        private void NewGame_CurrentTurnCompletedEvent(object sender, Events.CurrentTurnCompleted e)
        {
            AddAdditionnalInfosOnOutputTest(e);
        }

        private void AddAdditionnalInfosOnOutputTest(Object objectToJsonify)
        {
            output.WriteLine(JsonConvert.SerializeObject(objectToJsonify) + "\n");
        }
    }
}
