﻿using CardGame.Engine.Aggregates;
using System;

namespace CardGame.Commands
{
    public class PlayerJoinTheGame
    {
        public Guid GameId { get; private set; }
        public string PlayerPseudo { get; private set; }
        public bool IsBot { get; private set; }

        public PlayerJoinTheGame(Guid gameId, string playerPseudo, bool isBot)
        {
            GameId = gameId;
            PlayerPseudo = playerPseudo;
            IsBot = isBot;
        }
    }

    public class PlayerPlayCard
    {
        public int IndexSeatOfThePlayer { get; private set; }
        public PlayingCard Card { get; private set; }
        public Guid GameId { get; private set; }
 
        public PlayerPlayCard(int indexSeatOfThePlayer, PlayingCard card, Guid gameId)
        {
            IndexSeatOfThePlayer = indexSeatOfThePlayer;
            Card = card;
            GameId = gameId;
        }       
    }
}
