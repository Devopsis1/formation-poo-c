﻿using CardGame.Engine.Aggregates;
using System;
using System.Collections.Generic;

namespace CardGame.Events
{
    //Events who herits from EventArgs are public events !
    // if not ; there are internal events

    internal class CardsOnTheDeskAreShuffled
    {
        public List<PlayingCard> CardsOnTheDesk { get; private set; }

        public CardsOnTheDeskAreShuffled(List<PlayingCard> cardsOnTheDesk)
        {
            CardsOnTheDesk = cardsOnTheDesk;
        }
    }

    public class NewPlayerJoined : EventArgs
    {
        /// <summary>
        /// A player has join the game - event
        /// </summary>
        /// <param name="seatIndexAttributed">0 = NORTH, 1 = WEST, ...</param>
        /// <param name="isDealer"></param>
        /// <param name="isBot"></param>
        /// <param name="remainingPlacesCount"></param>
        public NewPlayerJoined(string playerPseudo, int seatIndexAttributed, bool isDealer, bool isBot, int remainingPlacesCount)
        {
            PlayerPseudo = playerPseudo;
            DatePlayerHasJoin = DateTime.Now;
            SeatIndexAttributed = seatIndexAttributed;
            IsDealer = isDealer;
            IsBot = isBot;
            RemainingPlacesCount = remainingPlacesCount;
        }

        public string PlayerPseudo { get; private set; }
        public DateTime DatePlayerHasJoin { get; private set; }
        public int SeatIndexAttributed { get; private set; }
        public bool IsDealer { get; private set; }
        public bool IsBot { get; private set; }

        /// <summary>
        /// Nombre de Places restantes pour acueillir d'autres joueurs 
        /// </summary>
        public int RemainingPlacesCount { get; private set; }
    }

    public class CardsAreDealed : EventArgs
    {
        public CardsAreDealed(int dealerIndexSeat, CardsInHands westPlayer, CardsInHands southPlayer, CardsInHands eastPlayer, CardsInHands northPlayer, int playerHaveToPlayIndexSeat)
        {
            NorthPlayer = northPlayer;
            WestPlayer = westPlayer;
            SouthPlayer = southPlayer;
            EastPlayer = eastPlayer;
            DealerIndexSeat = dealerIndexSeat;
            PlayerHaveToPlayIndexSeat = playerHaveToPlayIndexSeat;
        }

        public CardsInHands NorthPlayer { get; private set; }
        public CardsInHands WestPlayer { get; private set; }
        public CardsInHands SouthPlayer { get; private set; }
        public CardsInHands EastPlayer { get; private set; }

        public int DealerIndexSeat { get; private set; }
        public int PlayerHaveToPlayIndexSeat { get; private set; }
    }

    internal class PlayerHasCardPlayed
    {
        public PlayerHasCardPlayed(int playerHasCardPlayedIndexSeat, PlayingCard cardPlayed)
        {
            PlayerHasCardPlayedIndexSeat = playerHasCardPlayedIndexSeat;
            CardPlayed = cardPlayed;
        }

        public int PlayerHasCardPlayedIndexSeat { get; private set; }        

        public PlayingCard CardPlayed { get; private set; }
    }
    
    public class PlayerHaveToPlayChanged : EventArgs
    {
        public PlayerHaveToPlayChanged(int nextPlayerHaveToPlayIndexSeat, List<CardPlayedOnDesk> cardsOnDesk, int turnNumber)
        {
            NextPlayerHaveToPlayIndexSeat = nextPlayerHaveToPlayIndexSeat;
            CardsOnDesk = cardsOnDesk;
            TurnNumber = turnNumber;
        }

        public int TurnNumber { get; private set; }
        public int NextPlayerHaveToPlayIndexSeat { get; private set; }
        public List<CardPlayedOnDesk> CardsOnDesk { get; private set; }

    }

    public class CurrentTurnCompleted : EventArgs
    {
        public CurrentTurnCompleted(int seatWinner, int scoreAddedToTheWinner)
        {
            SeatWinner = seatWinner;
            ScoreAddedToTheWinner = scoreAddedToTheWinner;
        }

        public int SeatWinner { get; private set; }
        public int ScoreAddedToTheWinner { get; private set; }
    }

    public class CurrentGameCompleted : EventArgs
    {
        public CurrentGameCompleted(int northSouthTotalScore, int eastWestTotalScore)
        {
            NorthSouthTotalScore = northSouthTotalScore;
            EastWestTotalScore = eastWestTotalScore;
        }

        public int NorthSouthTotalScore { get; private set; }
        public int EastWestTotalScore { get; private set; }
        public string WinnerTeamName
        {
            get
            {
                if (NorthSouthTotalScore == EastWestTotalScore)
                    return "No winner, Equality!";
                else if (NorthSouthTotalScore > EastWestTotalScore)
                    return "Team North/South";
                else
                    return "Team East/West";
            }
        }
    }
}
