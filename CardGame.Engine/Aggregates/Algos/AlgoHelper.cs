﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CardGame.Engine.Aggregates.Algos
{
    public class CardDeckShuffler
    {
        /// <summary>
        /// Randomly shuffle a standard deck of 52 playing cards
        /// (four suits of thirteen cards each).
        /// </summary>
        /// <param name="rng">Pseudo random number generator to use for this shuffle</param>
        /// <returns>New deck of shuffled cards</returns>
        public static List<PlayingCard> Create(Random rng)
        {
            if (rng == null)
            {
                throw new ArgumentNullException(nameof(rng));
            }
            var deck = PlayingCard.GetAllPossibleCards().ToList();
            for (var i = 0; i < deck.Count; i++)
            {
                // Pick a random card that hasn't been shuffled yet
                var j = rng.Next(i, deck.Count);
                if (i == j)
                {
                    // Randomly chose to swap with itself, do nothing.
                    continue;
                }
                // Swap with the random card
                var temp = deck[j];
                deck[j] = deck[i];
                deck[i] = temp;
            }
            return deck;
        }

        public static IEnumerable<IEnumerable<PlayingCard>> CardsFor4Players(IEnumerable<PlayingCard> cardsToDeal)
        {
            List<PlayingCard> cardsForPlayer1 = new List<PlayingCard>();
            List<PlayingCard> cardsForPlayer2 = new List<PlayingCard>();
            List<PlayingCard> cardsForPlayer3 = new List<PlayingCard>();
            List<PlayingCard> cardsForPlayer4 = new List<PlayingCard>();

            int cardDistributedCount = 0;
            int cardToGiveInPassOne = 3;
            int cardToGiveInPassTwo = 2;
            int cardToGiveInPassThree = 3;

            //First loop
            cardsForPlayer1.AddRange(cardsToDeal.Take(cardToGiveInPassOne));
            cardDistributedCount += cardToGiveInPassOne;

            cardsForPlayer2.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassOne));
            cardDistributedCount += cardToGiveInPassOne;

            cardsForPlayer3.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassOne));
            cardDistributedCount += cardToGiveInPassOne;

            cardsForPlayer4.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassOne));
            cardDistributedCount += cardToGiveInPassOne;

            //Second loop
            cardsForPlayer1.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassTwo));
            cardDistributedCount += cardToGiveInPassTwo;

            cardsForPlayer2.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassTwo));
            cardDistributedCount += cardToGiveInPassTwo;

            cardsForPlayer3.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassTwo));
            cardDistributedCount += cardToGiveInPassTwo;

            cardsForPlayer4.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassTwo));
            cardDistributedCount += cardToGiveInPassTwo;

            //Third loop
            cardsForPlayer1.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassThree));
            cardDistributedCount += cardToGiveInPassThree;

            cardsForPlayer2.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassThree));
            cardDistributedCount += cardToGiveInPassThree;

            cardsForPlayer3.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassThree));
            cardDistributedCount += cardToGiveInPassThree;

            cardsForPlayer4.AddRange(cardsToDeal.Skip(cardDistributedCount).Take(cardToGiveInPassThree));
            cardDistributedCount += cardToGiveInPassThree;

            List<List<PlayingCard>> pilesFor4players = new List<List<PlayingCard>>(4)
            {
                cardsForPlayer1,
                cardsForPlayer2,
                cardsForPlayer3,
                cardsForPlayer4
            };

            return pilesFor4players;
        }
    }
}
