﻿using CardGame.Commands;
using CardGame.Events;
using System;

namespace CardGame.Engine.Aggregates
{
    public class Game : IGame
    {
        public Guid Id => throw new NotImplementedException();

        public event EventHandler<NewPlayerJoined> NewPlayerJoinedEvent;
        public event EventHandler<CardsAreDealed> CardsAreDealedEvent;
        public event EventHandler<PlayerHaveToPlayChanged> PlayerHaveToPlayChangedEvent;
        public event EventHandler<CurrentTurnCompleted> CurrentTurnCompletedEvent;
        public event EventHandler<CurrentGameCompleted> CurrentGameCompletedEvent;

        public void Handle(PlayerJoinTheGame cmd)
        {
            throw new NotImplementedException();
        }

        public void Handle(PlayerPlayCard cmd)
        {
            throw new NotImplementedException();
        }
    }
}
