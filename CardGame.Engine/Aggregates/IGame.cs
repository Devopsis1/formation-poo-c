﻿using System;
using CardGame.Commands;
using CardGame.Events;

namespace CardGame.Engine.Aggregates
{
    public interface IGame
    {
        /// <summary>
        /// Each game have to be unique, this id must be defined by the class who implement the IGame Interface
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// The player must use this command to join this game
        /// </summary>
        /// <remarks>
        /// rules : The first played take the seat "NORTH", the second "WEST"... anticlockwise way!
        /// each seat has a index number, see eIndexForSeat in ValueObject.cs file
        /// 
        /// If an abnormal use of this command is detected, an exception is raised
        /// and the system state does not change
        /// </remarks>
        void Handle(PlayerJoinTheGame cmd);

        /// <summary>
        /// The player must use this command to play a card
        /// </summary>
        /// <remarks>
        /// If an abnormal use of this command is detected, an exception is raised
        /// and the system state does not change
        /// </remarks>
        void Handle(PlayerPlayCard cmd);

        /// <summary>
        /// The system fire this event when a new player has join this game
        /// </summary>
        event EventHandler<NewPlayerJoined> NewPlayerJoinedEvent;
        /// <summary>
        /// The system fire this event when all cards are dealed
        /// </summary>
        event EventHandler<CardsAreDealed> CardsAreDealedEvent;
        /// <summary>
        /// The system fire this event each time when a player have to play
        /// </summary>
        event EventHandler<PlayerHaveToPlayChanged> PlayerHaveToPlayChangedEvent;
        /// <summary>
        /// The system fire this event when a turn is completed (= 4 cards are on the table)
        /// </summary>
        event EventHandler<CurrentTurnCompleted> CurrentTurnCompletedEvent;
        /// <summary>
        /// The system fire this event when all cards are played and the party is finished
        /// </summary>
        event EventHandler<CurrentGameCompleted> CurrentGameCompletedEvent;
    }
}