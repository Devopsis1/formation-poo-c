﻿using System;

namespace CardGame.DomainExceptions
{
    public enum eCodesRetour : int
    {
        NormalExitOfTheApplication=0,
        PlayerOverQuota,
        BadPlayerHasPlayed,
        PlayerCheat,
        RulesViolated
    }
    public class PlayerOverQuotaException : UnauthorizedAccessException
    {
        public eCodesRetour CodeRetour = eCodesRetour.PlayerOverQuota;

        public PlayerOverQuotaException(int quotaAuthorized) : base($"Player Quota is {quotaAuthorized}") { }
    }

    public class BadPlayerHasPlayedException : UnauthorizedAccessException
    {
        public eCodesRetour CodeRetour = eCodesRetour.BadPlayerHasPlayed;

        public BadPlayerHasPlayedException(int indexPlayerHasPlayed, int indexPlayerAutorizedToPlay) : base($"Player {indexPlayerHasPlayed} has played but only the player {indexPlayerAutorizedToPlay} can play now.") { }
    }

    public class PlayerCheatException : UnauthorizedAccessException
    {
        public eCodesRetour CodeRetour = eCodesRetour.PlayerCheat;

        public PlayerCheatException(int indexPlayerHasPlayed, string messageForThisCheater) : base($"Player {indexPlayerHasPlayed} is a cheater : {messageForThisCheater}") { }
    }

    public class RulesViolatedException : UnauthorizedAccessException
    {
        public eCodesRetour CodeRetour = eCodesRetour.RulesViolated;

        public RulesViolatedException(int indexPlayerHasPlayed, string messageExplainTheRuleViolated) : base($"Player {indexPlayerHasPlayed} is a cheater : {messageExplainTheRuleViolated}") { }
    }
}
